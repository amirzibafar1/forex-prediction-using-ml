# Forex Prediction Using ML 

## Name
Forecasting Directional Movement of Forex Using Hybrid Variational Mode Decomposition, Machine Learning and Bayesian Optimization
## Description
In this project, the exchange rate of the Euro to the US dollar, as one of The main currency pairs of the forex market has been examined. For this purpose, a hybrid method based on Time series decomposition, machine learning, and optimization methods is proposed. the goal is to predict the price for a day ahead will be raised, fall, or remain still. for this,
After calculating the Return price and labeling the data, by the variational mode decomposition method, We decompose time series into several high and low-frequency modes as features for machine learning methods.
The MLP method was selected from among the existing methods and its parameters have been optimized using Bayesian optimization. Finally, The proposed method was validated with other deep and machine learning methods.
<br />
This Project has been implemented both in Python and MATLAB (Final.py, Final.m)  <br />
app1.py: Comparison of Machine Learning Methods  <br />
app2.py: Comparison of Deep Learning Methods  <br />
app3.py: Hyperparameters Tuning using Bayesian Optimization  <br />

## Installation
Written In Python 3.9 & Matlab 2022 (not tested on other version)  <br />
•	numpy  <br />
•	pandas  <br />
•	yfinance  <br />
•	sklearn <br />
•	tensorflow, keras v
•	bayes_opt <br />
•	vmdpy <br />
•	lagmat <br />
**Note:** Internet connection is required to run
## Author and acknowledgment
Author: Amirreza Zibafar  <br />
email: Zibafara@ymail.com


