"""
Project: Comparison of Deep Learning Methods for Prediction Forex Movement 
@author: Amirreza Zibafar
@email: Zibafara@ymail.com

"""

# Import Libraries
import pandas as pd
import numpy as np
import yfinance as yf
from vmdpy import VMD  
from sklearn import model_selection
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM, Conv1D, MaxPooling1D, GRU,Bidirectional
from keras.utils import to_categorical




# Extract Yahoo Finance Data
until='2022-07-01'
since='2010-07-01'
symbol='EURUSD=X'

df= yf.download(symbol, since, until, auto_adjust=True)
df=pd.DataFrame(df)




# Labels Definition
def label_definition(x):
    if x > 0.1:
       return 2
    elif x > -0.1:
       return 0
    else:
       return 1
   

df['Return']= 100*(df['Close']- df['Close'].shift(1))/df['Close'].shift(1)   
df['Label']=df['Return']   
df['Label'] = df['Label'].apply(label_definition)
df=df[1:]



## Normalazaion

df['Return']=(df['Return']-df['Return'].min())/(df['Return'].max()-df['Return'].min())


## Decompositon
Numimf=10
imf, _, _= VMD(df['Return'], 1000, 0, Numimf, 0.01, 1, 5e-6)
imf=np.transpose(imf)



#Spilit Data to Train,Test 

Inputs=imf
Targets=(df['Label'].to_numpy())

Xtr, Xte, Ytr, Yte = model_selection.train_test_split(Inputs, Targets, train_size = 0.8,shuffle=False)



# creat 3D Data
def threeD_dataset (X, y, time_steps = 1):
    Xs, ys = [], []
    
    for i in range(len(X)-time_steps):
        v = X[i:i+time_steps, :]
        Xs.append(v)
        ys.append(y[i+time_steps])
        
    return np.array(Xs), np.array(ys)


TIME_STEPS = 10

TrainInputs, TrainTargets = threeD_dataset(Xtr,Ytr, TIME_STEPS)
TestInputs, TestTargets = threeD_dataset(Xte,Yte, TIME_STEPS)
TrainTargets= to_categorical(TrainTargets)
TestTargets= to_categorical(TestTargets)
print('X_train.shape: ', TrainInputs.shape)
print('y_train.shape: ', TrainTargets.shape)
print('X_test.shape: ', TestInputs.shape) 
print('y_test.shape: ', TestTargets.shape)




n_timesteps, n_features, n_outputs = TrainInputs.shape[1], TrainInputs.shape[2], TrainTargets.shape[1]

validation_split, epochs, batch_size = 0.1,100, 32

# LSTM
model = Sequential()
model.add(LSTM(100, input_shape=(n_timesteps, n_features)))
model.add(Dropout(0.5))
model.add(Dense(n_outputs, activation='softmax'))
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.01), 
              loss='categorical_crossentropy', metrics=['accuracy'])


history= model.fit(TrainInputs, TrainTargets, validation_split=validation_split, epochs=epochs, batch_size=batch_size, verbose=0)

TrainOutputs=model.predict(TrainInputs, batch_size=batch_size)
TestOutputs=model.predict(TestInputs, batch_size=batch_size)

_, TrainAccuracy = model.evaluate(TrainInputs, TrainTargets, batch_size=batch_size, verbose=0)
_, TestAccuracy = model.evaluate(TestInputs, TestTargets, batch_size=batch_size, verbose=0)


print('---------------------')
print('LSTM Performance:')
print('Train Acc: ', TrainAccuracy,'Test Acc: ', TestAccuracy)
print('---------------------')



# Bi-LSTM
model = Sequential()
model.add(Bidirectional(LSTM(100, input_shape=(n_timesteps, n_features))))
model.add(Dropout(0.5))
model.add(Dense(n_outputs, activation='softmax'))
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.01), 
              loss='categorical_crossentropy', metrics=['accuracy'])


history= model.fit(TrainInputs, TrainTargets, validation_split=validation_split, epochs=epochs, batch_size=batch_size, verbose=0)


TrainOutputs=model.predict(TrainInputs, batch_size=batch_size)
TestOutputs=model.predict(TestInputs, batch_size=batch_size)

_, TrainAccuracy = model.evaluate(TrainInputs, TrainTargets, batch_size=batch_size, verbose=0)
_, TestAccuracy = model.evaluate(TestInputs, TestTargets, batch_size=batch_size, verbose=0)


print('---------------------')
print('Bi-LSTM Performance:')
print('Train Acc: ', TrainAccuracy,'Test Acc: ', TestAccuracy)
print('---------------------')



# GRU

model = Sequential()
model.add(GRU(100, input_shape=(n_timesteps, n_features)))
model.add(Dropout(0.5))
model.add(Dense(n_outputs, activation='softmax'))
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.01), 
              loss='categorical_crossentropy', metrics=['accuracy'])

history= model.fit(TrainInputs, TrainTargets, validation_split=validation_split, epochs=epochs, batch_size=batch_size, verbose=0)


TrainOutputs=model.predict(TrainInputs, batch_size=batch_size)
TestOutputs=model.predict(TestInputs, batch_size=batch_size)

_, TrainAccuracy = model.evaluate(TrainInputs, TrainTargets, batch_size=batch_size, verbose=0)
_, TestAccuracy = model.evaluate(TestInputs, TestTargets, batch_size=batch_size, verbose=0)



print('---------------------')
print('GRU Performance:')
print('Train Acc: ', TrainAccuracy,'Test Acc: ', TestAccuracy)
print('---------------------')



# CNN-LSTM 
model = Sequential()
model.add(Conv1D(filters=32, kernel_size=3,
                                strides=1, padding="same",
                                activation="relu",
                                input_shape=(n_timesteps, n_features)))
model.add(Conv1D(filters=64, kernel_size=3,
                                strides=1, padding="same",
                                activation="relu",
                                input_shape=(n_timesteps, n_features)))

model.add(MaxPooling1D(pool_size=3,strides=1, padding='same'))
model.add(LSTM(100, input_shape=(n_timesteps, n_features)))
model.add(Dropout(0.5))
model.add(Dense(n_outputs, activation='softmax'))
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.01), 
              loss='categorical_crossentropy', metrics=['accuracy'])


history= model.fit(TrainInputs, TrainTargets, validation_split=validation_split, epochs=epochs, batch_size=batch_size, verbose=0)


TrainOutputs=model.predict(TrainInputs, batch_size=batch_size)
TestOutputs=model.predict(TestInputs, batch_size=batch_size)

_, TrainAccuracy = model.evaluate(TrainInputs, TrainTargets, batch_size=batch_size, verbose=0)
_, TestAccuracy = model.evaluate(TestInputs, TestTargets, batch_size=batch_size, verbose=0)



print('---------------------')
print('CNN-LSTM Performance:')
print('Train Acc: ', TrainAccuracy,'Test Acc: ', TestAccuracy)
print('---------------------')




