"""
Project: Hyperparameters Tuning using Bayesian Optimization
@author: Amirreza Zibafar
@email: Zibafara@ymail.com

"""

# Import Libraries
import numpy as np
import pandas as pd
import yfinance as yf
from sklearn import model_selection, neighbors, svm, neural_network,ensemble
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM, GRU, Bidirectional,MaxPooling1D, Conv1D
from keras.utils import to_categorical
from bayes_opt import BayesianOptimization
from vmdpy import VMD  
from lagmat import lagmat



# Extract Yahoo Finance Data
until='2022-07-01'
since='2010-07-01'
symbol='EURUSD=X'

df= yf.download(symbol, since, until, auto_adjust=True)
df=pd.DataFrame(df)



# Labels Definition
def label_definition(x):
    if x > 0.1:
       return 2
    elif x > -0.1:
       return 0
    else:
       return 1
   

df['Return']= 100*(df['Close']- df['Close'].shift(1))/df['Close'].shift(1)   
df['Label']=df['Return']   
df['Label'] = df['Label'].apply(label_definition)
df=df[1:]

print('------- Labeling Results------')
print('The number of times the price increase:',np.count_nonzero(df['Label']==2))
print('The number of times the price fall',np.count_nonzero(df['Label']==1))
print('The number of times the price remains unchanged:',np.count_nonzero(df['Label']==0))
print('---------------------')

## Normalazaion

df['Return']=(df['Return']-df['Return'].min())/(df['Return'].max()-df['Return'].min())

## Decompositon
Numimf=10
imf, _, _= VMD(df['Return'], 1000, 0, Numimf, 0, 1, 5e-6)
imf=np.transpose(imf)



# Lags Definition

lags=[1, 3, 5, 6, 8, 11, 15, 18, 23 ,30]
Range=list(range(max(lags)+1,len(df['Return'])))
lagPrice= lagmat(imf, lags=lags)


#Spilit Data to Train,Test 

Inputs= lagPrice[Range,:]
Targets=(df['Label'].to_numpy())[Range]

Xtr, Xte, Ytr, Yte = model_selection.train_test_split(Inputs, Targets, train_size = 0.8,shuffle=False)

#----------------- Machine Learning -----------------------------


# KNN
def KNNCostFunc(n_neighbors):
    n_neighbors=round(n_neighbors)
    MD = neighbors.KNeighborsClassifier(n_neighbors = n_neighbors)
    MD.fit(Xtr, Ytr)
    teAcc= MD.score(Xte, Yte)
    return teAcc

params ={'n_neighbors': (5, 500) }

bayes= BayesianOptimization(KNNCostFunc, params, random_state=0)
bayes.maximize(init_points=30, n_iter=30)

best_params = bayes.max['params']

print('KNN Best Parameters: ', best_params)


# SVM
def SVMCostFunc(C, kernel):
    kernel_name=['rbf', 'sigmoid']
    kernel = kernel_name[round(kernel)]
    MD = svm.SVC(C=C, kernel = kernel)
    MD.fit(Xtr, Ytr)
    teAcc = MD.score(Xte, Yte)
    return teAcc

params= {'C': (1, 100), 'kernel' : (0,1)}

bayes= BayesianOptimization(SVMCostFunc, params, random_state=0)

bayes.maximize(init_points=20, n_iter=20)
best_params = bayes.max['params']

kernel_name=['rbf', 'sigmoid']
best_params['kernel'] = kernel_name[round(best_params['kernel'])]

print('SVM Best Parameters: ', best_params)

# MLP
def MLPCostFunc(neurons1, neurons2, nlayer, activation ):
    activation_name=['logistic', 'tanh', 'relu']
    activation=activation_name[round(activation)]
    
    neurons1_list=[10,20, 32,64,128,256]
    neurons1=neurons1_list[round(neurons1)]

    neurons2_list=[10,20, 32,64,128,256]
    neurons2=neurons2_list[round(neurons2)]
    
    if round(nlayer) == 0:
        MD = neural_network.MLPClassifier(hidden_layer_sizes = neurons1, activation = activation,
                                  solver = 'adam', max_iter = 1500, learning_rate='adaptive', shuffle=(False))
    else:
        MD = neural_network.MLPClassifier(hidden_layer_sizes = (neurons1,neurons2), activation = activation,
                                  solver = 'adam', max_iter = 1500, learning_rate='adaptive', shuffle=(False))
    MD.fit(Xtr, Ytr)
    teAcc = MD.score(Xte, Yte)
    return teAcc

params= {'neurons1': (0, 5), 'neurons2': (0, 5), 
         'nlayer' : (0,1),  'activation': (0,2)}

bayes= BayesianOptimization(MLPCostFunc, params, random_state=0)


bayes.maximize(init_points=20, n_iter=20)
best_params = bayes.max['params']

activation_name=['logistic', 'tanh', 'relu']
best_params['activation'] = activation_name[round(best_params['activation'])]

neurons1=[8, 16, 32,64,128,256]
best_params['neurons1'] = neurons1[round(best_params['neurons1'])]

neurons2=[8, 16, 32,64,128,256]
best_params['neurons2'] = neurons2[round(best_params['neurons2'])]

nlayer=[1, 2]
best_params['nlayer'] = nlayer[round(best_params['nlayer'])]

print('MLP Best Parameters: ', best_params)

# Random Forest
def RFCostFunc(n_estimators, max_depth):
    n_estimators=round(n_estimators)
    max_depth=round(max_depth)
    MD=ensemble.RandomForestClassifier(n_estimators=n_estimators,max_depth=max_depth)
    MD.fit(Xtr, Ytr)
    teAcc = MD.score(Xte, Yte)
    return(teAcc)

params= {'n_estimators': (50, 500), 'max_depth': (5, 500) }

bayes= BayesianOptimization(RFCostFunc, params, random_state=0)
activation_name=['logistic', 'tanh', 'relu']
bayes.maximize(init_points=20, n_iter=20)
best_params = bayes.max['params']

print('RF Best Parameters: ', best_params)


#----------------- Deep Learning -----------------------------
# creat 3D Data
def threeD_dataset (X, y, time_steps = 1):
    Xs, ys = [], []
    
    for i in range(len(X)-time_steps):
        v = X[i:i+time_steps, :]
        Xs.append(v)
        ys.append(y[i+time_steps])
        
    return np.array(Xs), np.array(ys)


TIME_STEPS = 10

TrainInputs, TrainTargets = threeD_dataset(Xtr,Ytr, TIME_STEPS)
TestInputs, TestTargets = threeD_dataset(Xte,Yte, TIME_STEPS)
TrainTargets= to_categorical(TrainTargets)
TestTargets= to_categorical(TestTargets)


n_timesteps, n_features, n_outputs = TrainInputs.shape[1], TrainInputs.shape[2], TrainTargets.shape[1]


def CostFunc(neurons, layer_name, filters, dropout, learning_rate,  batch_size ):
    
    name=['LSTM','GRU','Bidirectional-LSTM','CNN-LSTM']
    Layer_name = name[round(layer_name)]
    
    neurons_list=[32,64,128,256,512]
    neurons=neurons_list[ round(neurons)]
    
    filters_list=[32,64,128,256]
    filters=filters_list[ round(filters)]

    learning_rate_list=[ 0.001, 0.005, 0.01, 0.1]
    learning_rate=learning_rate_list[round(learning_rate)]
    
    batch_size_list=[8, 16, 32, 64, 128]
    batch_size=batch_size_list[round(batch_size)]

    epochs = 50

    
    model = Sequential()
    if Layer_name=='Bidirectional-LSTM':
        model.add(Bidirectional(LSTM(neurons, input_shape=(n_timesteps, n_features))))
    elif Layer_name=='GRU':
        model.add(GRU(neurons, input_shape=(n_timesteps, n_features)))
    elif Layer_name=='LSTM':
        model.add(LSTM(neurons, input_shape=(n_timesteps, n_features)))
    else:
        model.add(Conv1D(filters=filters, kernel_size=3,
                                       strides=1, padding="same",
                                       activation="relu",
                                       input_shape=(n_timesteps, n_features)))
        model.add(Conv1D(filters=2*filters, kernel_size=3,
                                       strides=1, padding="same",
                                       activation="relu",
                                       input_shape=(n_timesteps, n_features)))
        model.add(MaxPooling1D(pool_size=3,strides=1, padding='same'))
        model.add(LSTM(neurons, input_shape=(n_timesteps, n_features)))
    
    model.add(Dropout(dropout))
    model.add(Dense(n_outputs, activation='softmax'))

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate), 
                  loss='categorical_crossentropy', metrics=['accuracy'])
    
    model.fit(TrainInputs, TrainTargets,validation_split=0.1 ,epochs=epochs, batch_size=batch_size, verbose=0)
    _, TestAccuracy = model.evaluate(TestInputs, TestTargets, batch_size=batch_size, verbose=0)
    
    
    return TestAccuracy



params ={
    'neurons': (0, 4),
    'layer_name':(0, 3),
    'filters' :(0,3),
    'dropout':(0.1, 0.5),
    'learning_rate':(0, 3),
    'batch_size':(0, 4),
       }

bayes= BayesianOptimization(CostFunc, params, random_state=0)
bayes.maximize(init_points=10, n_iter=10)

best_params = bayes.max['params']

Layername=['LSTM','GRU','Bidirectional-LSTM', 'CNN-LSTM']
best_params['layer_name'] = Layername[round(best_params['layer_name'])]

neurons=[32,64,128,256,512]
best_params['neurons'] = neurons[round(best_params['neurons'])]

filters=[32,64,128,256]
best_params['filters'] = filters[round(best_params['filters'])]

learning_rate=[0.001, 0.005, 0.01, 0.1]
best_params['learning_rate'] = learning_rate[round(best_params['learning_rate'])]

batch_size=[8, 16, 32, 64, 128]
best_params['batch_size'] = batch_size[round(best_params['batch_size'])]

print('Best Parameters: ', best_params)


