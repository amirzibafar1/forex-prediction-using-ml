
"""
Project: Prediction Forex Movement Using MLP (Best Model)
@author: Amirreza Zibafar
@email: Zibafara@ymail.com

"""

# Import Libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import yfinance as yf
from sklearn import model_selection, neural_network
from statsmodels.graphics.tsaplots import plot_acf
from vmdpy import VMD  
from lagmat import lagmat
from sklearn.metrics import confusion_matrix


# Extract Yahoo Finance Data
until='2022-07-01'
since='2010-07-01'
symbol='EURUSD=X'

df= yf.download(symbol, since, until, auto_adjust=True)
df=pd.DataFrame(df)


# Plot Data
plt.figure()
plt.plot(df['Close'])
plt.title('EUR-USD Close Price')
plt.xlabel('Timeline')
plt.ylabel('Close Price')
plt.show()



# Labels Definition
def label_definition(x):
    if x > 0.1:
       return 2
    elif x > -0.1:
       return 0
    else:
       return 1
   

df['Return']= 100*(df['Close']- df['Close'].shift(1))/df['Close'].shift(1)   
df['Label']=df['Return']   
df['Label'] = df['Label'].apply(label_definition)
df=df[1:]



print('------- Labeling Results------')
print('The number of times the price increase:',np.count_nonzero(df['Label']==2))
print('The number of times the price fall',np.count_nonzero(df['Label']==1))
print('The number of times the price remains unchanged:',np.count_nonzero(df['Label']==0))
print('---------------------')

## Normalazaion

df['Return']=(df['Return']-df['Return'].min())/(df['Return'].max()-df['Return'].min())


## Decompositon
Numimf=10
imf, _, _= VMD(df['Return'], 1000, 0, Numimf, 0, 1, 5e-6)
imf=np.transpose(imf)

plt.figure()

for i in range(0,Numimf):
    plt.subplot(Numimf,1,i+1)
    plt.plot(imf[:,i])
    if i==0:
        plt.title(' Variational Mode Decomposition')

plt.show()


# Lags Definition
plot_acf(x=df['Return'], lags=30, zero=False) 
plt.show()

lags=[1, 3, 5, 6, 8, 11, 15, 18, 23 ,30]
Range=list(range(max(lags)+1,len(df['Return'])))
lagPrice= lagmat(imf, lags=lags)


#Spilit Data to Train,Test 

Inputs= lagPrice[Range,:]
Targets=(df['Label'].to_numpy())[Range]

Xtr, Xte, Ytr, Yte = model_selection.train_test_split(Inputs, Targets, train_size = 0.8,shuffle=False)




# MLP
MD = neural_network.MLPClassifier(hidden_layer_sizes = (20,), activation = 'relu',
                                  solver = 'lbfgs', max_iter = 5000 )
MD.fit(Xtr, Ytr)
trAcc = MD.score(Xtr, Ytr)
teAcc = MD.score(Xte, Yte)

print('---------------------')
print('MLP Performance:')
print('Train Acc: ', trAcc,'Test Acc: ', teAcc)
print('---------------------')



# Prediction for Tomorrow 

lagPrice= lagmat(imf, lags=(list(np. array(lags)-1)))
Xpr= lagPrice[-1,:]
Xpr=Xpr.reshape(1, -1)

TrainOutputs=MD.predict(Xtr)
TestOutputs=MD.predict(Xte)
PredOutputs=MD.predict(Xpr)

print("***************************************")
if PredOutputs==2:
    print('Prediction for Tomorrow: Raise')
elif PredOutputs==1:
    print('Prediction for Tomorrow: Fall')
else:
    print('Prediction for Tomorrow: Neutral')
    
print("***************************************") 


# Confusion Matrix 
cf_test = confusion_matrix(TestOutputs,Yte)

plt.figure()
ax = sns.heatmap(cf_test, annot=True, cmap='Blues')
ax.set_title('\nConfusion Matrix ');
ax.set_xlabel('Predicted')
ax.set_ylabel('Actual');

ax.xaxis.set_ticklabels(['Neutral','Raise','Fall'])
ax.yaxis.set_ticklabels(['Neutral','Raise','Fall'])
plt.show()

  








