clc;
clear;
close all;

%% Get Data from Yahoo Finance
pyrun("import pandas as pd")
pyrun("import yfinance as yf")
pyrun("df= yf.download('EURUSD=X', '2010-07-01','2022-07-01', auto_adjust=True)")
pyrun("df=pd.DataFrame(df)")
pyrun("df.to_excel('Data.xlsx')")

%% Load Data
opts = detectImportOptions('Data.xlsx');
Data= readtable("Data.xlsx",opts);
Data = table2timetable(Data);

%% Price Return Calculation
Price=Data.Close;                     
LagPrice=lagmatrix(Price,1);          
Ret=100.*(Price-LagPrice)./LagPrice;  

Ret=Ret(2:end);                       
Date=Data.Date(2:end);
n=numel(Ret); 

%% Plots Data
figure;
subplot(2,1,1)
plot(Date,Price(2:end))
xlabel('TimeLine')
ylabel('Close Price')
title('EUR-USD Historical Price')
grid on

subplot(2,1,2)
plot(Date,Ret)
xlabel('TimeLine')
ylabel('Price Return (%)')
title({['Max :',num2str(max(Ret)),' Min :',num2str(min(Ret)),...
        ' Average :',num2str(mean(Ret))]})
grid on

%% Labels Definition
Label=zeros(n,1);
for i=1:n
    if Ret(i)>0.1
        Label(i)=2;
    elseif Ret(i)<-0.1
        Label(i)=1;
    else
        Label(i)=0;
    end
end

Label=categorical(Label,[0 1 2],{'Neutral' 'Fall' 'Raise'});

disp('Labels name & Number of Labels:')
summary(Label)

%% Normalazation
Ret=(Ret-min(Ret))/(max(Ret)-min(Ret));

%% Find Best Lags
figure;
autocorr(Ret,30);

C=autocorr(Ret,30);
C=abs(C(2:end));
[~,BestLags_idx]=sort(C,'descend');
BestLags_idx=BestLags_idx(1:10);
BestLags=sort(BestLags_idx);
Range=max(BestLags)+1:n;

%% Decomposition
NumIMFs=10;
imf=vmd(Ret,'NumIMFs',NumIMFs);

[p,q] = ndgrid(Date,1:size(imf,2));
figure;
plot3(p,q,imf)
grid on
title('Euro-USD Return Price Decomposition')
xlabel('Time Values')
ylabel('Mode Number')
zlabel('Mode Amplitude')


Inputs=lagmatrix(imf,BestLags);
Inputs=Inputs(Range,:);
Targets=Label(Range);

%% Spilit Data to Train & Test
ts=round(0.2*size(Inputs,1));             
TrainInputs=Inputs(1:end-ts,:);
TrainTargets=Targets(1:end-ts);

TestInputs=Inputs(end-ts+1:end,:);
TestTargets=Targets(end-ts+1:end);


%% Neural Network Classification
net = fitcnet(TrainInputs,TrainTargets,'LayerSizes',20);
TrainAccuracy = 1 - loss(net,TrainInputs,TrainTargets);
TestAccuracy = 1 - loss(net,TestInputs,TestTargets);

disp('Neural Network Classification Performance :')
fprintf('Train Accuracy: %f  & Test Accuracy: %f \n',TrainAccuracy,TestAccuracy);

%% Confusion Matrix 
confusionchart(TestTargets,net.predict(TestInputs))
title('Confusion Matrix')

%% Prediction For Tomorro
PredInputs=lagmatrix(imf,BestLags-1);
PredInputs=PredInputs(end,:);
PredOutput=net.predict(PredInputs);

disp('Prediction For Tomorrow:')
disp(PredOutput)

