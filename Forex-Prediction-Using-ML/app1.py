
"""
Project: Comparison of Machine Learning Methods for Prediction Forex Movement 
@author: Amirreza Zibafar
@email: Zibafara@ymail.com

"""

# Import Libraries
import numpy as np
import pandas as pd
import yfinance as yf
from sklearn import model_selection, neighbors, svm, neural_network,ensemble
from vmdpy import VMD  
from lagmat import lagmat



# Extract Yahoo Finance Data
until='2022-07-01'
since='2010-07-01'
symbol='EURUSD=X'

df= yf.download(symbol, since, until, auto_adjust=True)
df=pd.DataFrame(df)



# Labels Definition
def label_definition(x):
    if x > 0.1:
       return 2
    elif x > -0.1:
       return 0
    else:
       return 1
   

df['Return']= 100*(df['Close']- df['Close'].shift(1))/df['Close'].shift(1)   
df['Label']=df['Return']   
df['Label'] = df['Label'].apply(label_definition)
df=df[1:]



## Normalazaion

df['Return']=(df['Return']-df['Return'].min())/(df['Return'].max()-df['Return'].min())


## Decompositon
Numimf=10
imf, _, _= VMD(df['Return'], 1000, 0, Numimf, 0, 1, 5e-6)
imf=np.transpose(imf)


# Lags Definition

lags=[1, 3, 5, 6, 8, 11, 15, 18, 23 ,30]
Range=list(range(max(lags)+1,len(df['Return'])))
lagPrice= lagmat(imf, lags=lags)


#Spilit Data to Train,Test 

Inputs= lagPrice[Range,:]
Targets=(df['Label'].to_numpy())[Range]

Xtr, Xte, Ytr, Yte = model_selection.train_test_split(Inputs, Targets, train_size = 0.8,shuffle=False)



# KNN
MD = neighbors.KNeighborsClassifier(n_neighbors = 482)
MD.fit(Xtr, Ytr)
trAcc= MD.score(Xtr, Ytr)
teAcc= MD.score(Xte, Yte)

print('---------------------')
print('KNN Performance:')
print('Train Acc:', trAcc, 'Test Acc:', teAcc)
print('---------------------')



# SVM
MD = svm.SVC(C=96, kernel = 'rbf')
MD.fit(Xtr, Ytr)
trAcc = MD.score(Xtr, Ytr)
teAcc = MD.score(Xte, Yte)

print('---------------------')
print('SVM Performance:')
print('Train Acc: ', trAcc,'Test Acc: ', teAcc)
print('---------------------')


# MLP
MD = neural_network.MLPClassifier(hidden_layer_sizes = (20,), activation = 'relu',
                                  solver = 'lbfgs', max_iter = 5000 )
MD.fit(Xtr, Ytr)
trAcc = MD.score(Xtr, Ytr)
teAcc = MD.score(Xte, Yte)

print('---------------------')
print('MLP Performance:')
print('Train Acc: ', trAcc,'Test Acc: ', teAcc)
print('---------------------')



# Random Forest
MD=ensemble.RandomForestClassifier(n_estimators=472,max_depth=356)
MD.fit(Xtr, Ytr)
trAcc = MD.score(Xtr, Ytr)
teAcc = MD.score(Xte, Yte)
print('---------------------')
print('Random Forest Performance:')
print('Train Acc: ', trAcc,'Test Acc: ', teAcc)
print('---------------------')










